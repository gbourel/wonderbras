import {
  AmbientLight,
  BackSide,
  BoxBufferGeometry,
  DirectionalLight,
  LinearFilter,
  Mesh,
  PerspectiveCamera,
  ShaderLib,
  ShaderMaterial,
  Scene,
  sRGBEncoding,
  TextureLoader,
  Vector3,
  WebGLRenderer
} from 'three';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js';
import { OrbitControls } from './SimpleOrbitControls.js';

// let width = window.innerWidth /2;
// let height = window.innerHeight /2;

class ArmSimulator {
  constructor(parent) {
    this.prevTime = 0;
    this.scene = new Scene();
    this.width = parent.offsetWidth;
    this.height = parent.offsetHeight;
    this.camera = new PerspectiveCamera( 75, this.width / this.height, 0.01, 50 );
    this.renderer = new WebGLRenderer({antialias: true});
    this.renderer.autoClearColor = false;

    this.renderer.physicallyCorrectLights = true;
    this.renderer.outputEncoding = sRGBEncoding;
    this.renderer.setClearColor( 0xcccccc );
    this.renderer.setPixelRatio( window.devicePixelRatio );
    this.renderer.setSize( this.width, this.height );
    parent.appendChild( this.renderer.domElement );
    
    const light1  = new AmbientLight(0xFFFFFF, 0.8);
    light1.name = 'ambient_light';
    this.scene.add( light1 );
    
    const light2  = new DirectionalLight(0xFFFFFF, 1.5);
    light2.position.set(-0.5, 0, 0.866); // ~60º
    light2.name = 'main_light';
    this.scene.add( light2 );
    
    this.camera.position.z = -0.4;
    this.camera.position.y = 0.4;
    this.camera.position.x = -0.4;
    this.camera.zoom = 1.0;
    this.camera.lookAt(new Vector3(0, 0.2, 0));
    this.camera.updateProjectionMatrix();
    
    this.controls = new OrbitControls( this.camera, this.renderer.domElement );
    // this.controls.screenSpacePanning = true;
    this.controls.enableZoom = false;
    this.controls.enablePan = false;
    
    this.bones = {};
    
    this.bgMesh = null;
    this.bgScene = new Scene();
    this.loadBackground()

    this.theta = .0;
    this.alpha1 = -.537;
    this.alpha2 = -1.439;
    this.alpha3 = 1.068;
    this.loadModel('robotDobot_s.glb');
  }

  loadBackground() {
    const loader = new TextureLoader();
    const texture = loader.load('warehouse.jpg');
    texture.magFilter = LinearFilter;
    texture.minFilter = LinearFilter;
  
    const shader = ShaderLib.equirect;
      const material = new ShaderMaterial({
      fragmentShader: shader.fragmentShader,
      vertexShader: shader.vertexShader,
      uniforms: shader.uniforms,
      depthWrite: false,
      side: BackSide,
    });
    material.uniforms.tEquirect.value = texture;
    const plane = new BoxBufferGeometry(2, 2, 2);
    this.bgMesh = new Mesh(plane, material);
    this.bgScene.add(this.bgMesh);
  }

  initBones(obj) {
    if(obj && obj.children) {
      for(let child of obj.children) {
        if(child.name.startsWith('Bone')) {
          this.bones[child.name] = child;
        }
        this.initBones(child);
      }
    }
  }

  loadModel(model) {
    const loader = new GLTFLoader();
    loader.load(model, (gltf) => {
      
      console.info('GLTF', gltf);
    
      this.scene.add( gltf.scene );
    
      this.initBones(gltf.scene);
    }, undefined, function ( error ) {
      console.error( error );
    });
  }
  
  update(time) {
    // const dt = (time - this.prevTime) / 1000;
    this.controls.update();

    // Bone
    // Bone001 1st
    // Bone003 2nd 
    // Bone004 3rd 
    // Bone005 Z rotation
    if(this.bones['Bone005']) { // FIXME
      let delta = this.theta - this.bones['Bone005'].rotation.y; 
      if(Math.abs(delta) > 0.01) {
        if(delta > 0) {
          this.bones['Bone005'].rotation.y += 0.01;
        } else {
          this.bones['Bone005'].rotation.y -= 0.01;
        }
      }
      delta = this.alpha1 - this.bones['Bone001'].rotation.x; 
      if(Math.abs(delta) > 0.01) {
        if(delta > 0) {
          this.bones['Bone001'].rotation.x += 0.01;
        } else {
          this.bones['Bone001'].rotation.x -= 0.01;
        }
      }
      delta = this.alpha2 - this.bones['Bone003'].rotation.x; 
      if(Math.abs(delta) > 0.01) {
        if(delta > 0) {
          this.bones['Bone003'].rotation.x += 0.01;
        } else {
          this.bones['Bone003'].rotation.x -= 0.01;
        }
      }
      delta = this.alpha3 - this.bones['Bone004'].rotation.x; 
      if(Math.abs(delta) > 0.01) {
        if(delta > 0) {
          this.bones['Bone004'].rotation.x += 0.01;
        } else {
          this.bones['Bone004'].rotation.x -= 0.01;
        }
      }
    }

    if(this.bgScene && this.bgMesh) {
      this.bgMesh.position.copy(this.camera.position);
      this.renderer.render(this.bgScene, this.camera);
    }
    this.renderer.render( this.scene, this.camera );
    this.prevTime = time;
  }
}

export { ArmSimulator };
