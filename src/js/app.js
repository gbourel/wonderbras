import { ArmSimulator } from './ArmSimulator.js';

console.info('Init simulator');
let a = new ArmSimulator(document.getElementById('simulation'));

function animate(time) {
  requestAnimationFrame( animate );
  a.update(time)
}
animate();

let ti = document.getElementById('theta-input');
ti.value = a.theta;
ti.addEventListener('change', (evt) => {
  a.theta = evt.target.value;
});
let a1i = document.getElementById('alpha1-input');
a1i.value = a.alpha1;
a1i.addEventListener('change', (evt) => {
  a.alpha1 = evt.target.value;
});
let a2i = document.getElementById('alpha2-input');
a2i.value = a.alpha2;
a2i.addEventListener('change', (evt) => {
  a.alpha2 = evt.target.value;
});
let a3i = document.getElementById('alpha3-input');
a3i.value = a.alpha3;
a3i.addEventListener('change', (evt) => {
  a.alpha3 = evt.target.value;
});

a.theta = 2;
// Demo movements
setInterval(() => {
  if(a.theta < 0) {
    a.theta = 2;
    a.alpha1 = 0.2;
    a.alpha2 = -1.5;
    a.alpha3 = 0.5;
  } else {
    a.theta = -0.5;
    a.alpha1 = -1.5;
    a.alpha2 = 0.0;
    a.alpha3 = 0.9;
  }
}, 5000);
